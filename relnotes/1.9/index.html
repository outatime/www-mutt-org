<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 1.9 Release Notes</title>
  </head>

  <body>
    <h2>Mutt 1.9 Release Notes</h2>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h3>OpenSSL partial chain support</h3>

    <p>
      <a href="http://www.mutt.org/doc/manual/#ssl-verify-partial-chains"
      >$ssl_verify_partial_chains</a> enables the
      X509_V_FLAG_PARTIAL_CHAIN flag for OpenSSL certificate
      verification.  This allows a certificate to be validated without
      trusting the entire chain: an intermediate or leaf certificate
      in the trusted store is enough to validate the cert.
    </p>
    <p>
      When set, a <em>(s)kip</em> choice is added to the certificate
      prompt:<br />
      <img src="openssl-skip.png" /><br />
      By selecting
      <em>(s)kip</em>, an intermediate or leaf node further down the
      chain can be saved to the
      <a href="http://www.mutt.org/doc/manual/#certificate-file"
      >$certificate_file</a> using the <em>(a)ccept always</em>
      choice.
    </p>
    <p>
      Note this option is only available for OpenSSL and requires
      version 1.0.2b or newer.
    </p>

    <h3>Redrawing improvements</h3>
    <p>
      The redrawing engine was rearchitected, allowing for a couple user
      visible improvements.
    </p>
    <p>
      The first improvement is multi-line prompts:<br />
      <img src="multiline-prompt.png" />
    </p>
    <p>
      The second improvement is the ability to process redraws while
      inside the line editor.  Rather than the whole screen becoming
      distorted until after the prompt is exited, the screen will be
      reflowed and redrawn:<br />
      <video>
        <source src="mutt-editor-resize.ogv" type="video/ogg" />
      </video>
    </p>

    <h3>Compose menu colors</h3>
    <p>
      The compose menu header labels and Security status can now
      be <a href="http://www.mutt.org/doc/manual/#color"
      >colored</a>.  This is controlled by the "color compose header"
      and "color compose security_{encrypt|sign|both|none}" commands:
    </p>
    <pre>
color compose header green default
color compose security_none red default
    </pre>
    <p>
      <img src="compose-colors.png" />
    </p>

    <h3>Pager header partial coloring</h3>
    <p>
      <a href="http://www.mutt.org/doc/manual/#header-color-partial"
      >$header_color_partial</a> changes how "color header" matches are
      applied.  If unset, the default, matches will color the entire
      header.  When set, only the matched string will be colored.
      This, for example, allows just header labels to be colored:
    </p>
    <pre>
set header_color_partial
color header default green '^[^[:blank:]:]*:'
color hdrdefault yellow default
    </pre>
    <p>
      <img src="pager-header-color.png" />
    </p>

    <h3>History ring duplicate removal</h3>
    <p>
      By default, Mutt will not add consecutively repeating commands to the
      history ring.  When
      <a href="http://www.mutt.org/doc/manual/#history-remove-dups"
      >$history_remove_dups</a> is set, Mutt will also scan and
      remove duplicate entries from the entire ring when adding a new entry.
    </p>

    <h3>IMAP improvements</h3>
    <p>
      Header downloading was revamped to support out of order and
      missing MSNs in the results.  This is an uncommon but legal
      scenario, which previously could result in mutt hanging while
      downloading headers.
    </p>
    <p>
      As a result of these improvements, body caching cleanup should be
      faster.  Performance may be acceptable enough to leave
      <a href="http://www.mutt.org/doc/manual/#message-cache-clean"
      >$message_cache_clean</a> set all the time.
    </p>
    <p>
      <a href="http://www.mutt.org/doc/manual/#imap-poll-timeout"
      >$imap_poll_timeout</a> controls how long Mutt will wait when
      polling a mailbox until timing out and closing the connection.
      The default is 15 seconds.
    </p>

    <h3>Self encryption</h3>
    <p>
      The new configuration variables
      <a href="http://www.mutt.org/doc/manual/#pgp-self-encrypt"
      >$pgp_self_encrypt</a> and
      <a href="http://www.mutt.org/doc/manual/#smime-self-encrypt"
      >$smime_self_encrypt</a> enable adding your own key when sending an
         encrypted PGP or S/MIME message.
      <a href="http://www.mutt.org/doc/manual/#pgp-self-encrypt-as"
      >$pgp_self_encrypt_as</a> and
      <a href="http://www.mutt.org/doc/manual/#smime-self-encrypt-as"
      >$smime_self_encrypt_as</a> specify the key to add.
    </p>
    <p>
      <a href="http://www.mutt.org/doc/manual/#postpone-encrypt"
      >$postpone_encrypt</a> previously checked the value of
      <a href="http://www.mutt.org/doc/manual/#postpone-encrypt-as"
      >$postpone_encrypt_as</a>, but this doesn't support users of
      both PGP and S/MIME well.  $postpone_encrypt will now check the
      $pgp_self_encrypt_as and $smime_self_encrypt_as configuration
      variables first, falling back to $postpone_encrypt_as if they
      are unset.
    </p>

    <h3>Forwarded messages attribution</h3>
    <p>
      <a href="http://www.mutt.org/doc/manual/#forward-attribution-intro"
      >$forward_attribution_intro</a> and
      <a href="http://www.mutt.org/doc/manual/#forward-attribution-trailer"
      >$forward_attribution_trailer</a> can now be used to customize
      the (previously hardcoded) attribution messages when including a
      forwarded message in the body.
    </p>

    <h3>New pattern operators for immediate parent and children</h3>
    <p>
      Sometimes it is useful to select messages based on their
      immediate parent or children.  <b>~&lt;(PATTERN)</b> and
      <b>~&gt;(PATTERN)</b> do this.  The PATTERN is matched against a
      message's immediate parent or children, respectively.
    </p>
    <p>
      So for example, <em>~&lt;(~P)</em> will match replies to your messages,
      while <em>~&gt;(~P)</em> will match messages that you replied to.
      <em>!~&lt;(~A)</em> will match root messages in a thread, while
      <em>!~&gt;(~A)</em> will match leaf messages.
    </p>
    <p>
      Like the <b>~(PATTERN)</b> operator, messages must be sorted by thread
      for the operator to work.
    </p>

    <h3>Nested encryption support in the attachment menu</h3>
    <p>
      Sometimes the encrypted part of a message is nested, or sometimes
      an encrypted message may be attached.  Previously, mutt would decrypt and show
      the nested content in the pager, but the atttachment menu would not
      show any of the nested parts.  This made it tricky to save or operate
      on any of those parts.
    </p>
    <p>
      Here is 1.8.3 with a nested encrypted message:<br />
      <img src="mutt-1.8.3-nested-encryption.png" /><br />
      And here is 1.9.0 with the same message:<br />
      <img src="mutt-1.9.0-nested-encryption.png" />
    </p>

    <h3>Command to query an attachment's MIME type</h3>
    <p>
      When creating a new attachment, Mutt consults the mime.types
      file to determine the correct MIME type.  If the attachment's
      extension isn't found, or if the attachment doesn't have an
      extension,
      <a href="http://www.mutt.org/doc/manual/#mime-type-query-command"
      >$mime_type_query_command</a> can be used to specify a program
      to run to determine the MIME type.  Two suggested programs are
      <em>&quot;xdg-mime query filetype&quot;</em> or <em>&quot;file
      -bi&quot;</em>.
    </p>
    <p>
      To run the command before (or instead of) the mime.types file
      lookup,
      <a href="http://www.mutt.org/doc/manual/#mime-type-query-first"
      >$mime_type_query_first</a> should be set.  Mutt will then only
      consult the mime.types file if the query command does not output
      a MIME type.
    </p>
  </body>
</html>
