<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 1.14 Release Notes</title>
    <meta charset="utf-8"/>
  </head>

  <body>
    <h1>Mutt 1.14 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h2>Background Editing</h2>
    <p>
      This feature requires either 1) a graphical editor or 2) a terminal
      multiplexer (e.g. GNU Screen or tmux) combined with a script
      such as
      <a href="https://gitlab.com/muttmua/mutt/-/blob/master/contrib/bgedit-screen-tmux.sh"
      >contrib/bgedit-screen-tmux.sh</a>.  With one of those, enabling
      <a href="/doc/manual/#background-edit">$background_edit</a>
      allows you to continue using Mutt will editing.  You can view
      messages, change mailboxes, even launch other message
      composition sessions.  For details, please be sure to read the
      <a href="/doc/manual/#bgedit">Background Editing</a>
      section of the manual.
    </p>

    <p>
      After your editor launches, Mutt will display a landing page:
    </p>

    <p>
      <img src="bgedit-landing.png">
    </p>

    <p>
      When you finish editing, the landing page will automatically
      resume message composition, returning to the
      <a href="/doc/manual/#compose-menu">compose menu</a>.
    </p>

    <p>
      However, you can also exit the compose menu using the
      <code class="literal">&lt;exit&gt;</code> function, by default
      bound to '<code class="literal">q</code>'.  This will return to
      wherever you started the composition session.
    </p>

    <p>
      When you are ready to resume editing, invoke the
      <code class="literal">&lt;background-compose-menu&gt;</code>
      function, by default bound to '<code class="literal">B</code>'.
      If there is a single editing session, and the editor has
      finished running, Mutt will automatically resume it.  Otherwise,
      the list of backgrounded message composition sessions will be
      displayed:
    </p>

    <p>
      <img src="bgedit-menu.png">
    </p>

    <p>
      Press return on a finished session to resume that session and
      return to the compose menu.
    </p>

    <h2>IMAP Deflate Compression</h2>
    <p>
      <a href="/doc/manual/#imap-deflate">$imap_deflate</a> enables
      compression if the IMAP server supports it.  This defaults off
      for now, so be sure to give it a try and report if you have any
      problems (or if you have good results).
    </p>

    <h2>Fcc to Multiple Mailboxes</h2>
    <p>
      To save Fcc copies to multiple mailboxes, set
      <a href="/doc/manual/#fcc-delimiter">$fcc_delimiter</a> to the separator string
      to use, for example &quot;,&quot;.  Then append multiple mailboxes
      to your <a href="/doc/manual/#record">$record</a> or
      <a href="/doc/manual/#fcc-hook">fcc-hook</a> values, separated
      by that delimeter:
    </p>
    <pre>
      set fcc_delimiter = ','
      set record = '=sent,=private'
      fcc-hook 'foo@example\.com$' '+one,+two'
    </pre>
    <p>
      Make sure you set the $fcc_delimiter <b>before</b> defining
      $record or the fcc-hook.  This ensures Mutt will properly expand
      each mailbox at the time of assignment.
    </p>
    <p>
      If you assign multiple mailboxes to $record, be aware that
      its <a href="/doc/manual/#shortcuts">mailbox shortcut</a>,
      &quot;<code class="literal">&lt;</code>&quot; won't be usable in
      other places.  The Fcc code understands multiple mailboxes, but
      <code class="literal">&lt;change-folder&gt;</code> and other functions do not.
    </p>
    <p>
      Also, note that fcc-save-hook is not designed to work with
      multiple mailboxes.  With $fcc_delimiter defined, it's better to use
      separate fcc-hook and save-hook commands.
    </p>

    <h2>Mailbox Labels and Polling</h2>
    <p>
      Two flags have been added to the
      <a href="/doc/manual/#mailboxes">mailboxes</a> command this release:
      <code class="literal">-label</code>/<code class="literal">-nolabel</code>,
      and <code class="literal">-poll</code>/<code class="literal">-nopoll</code>.
    </p>
    <p>
      <code class="literal">-label</code> specifies an alternate
      string to display in the sidebar or mailbox browser menu, in
      place of the mailbox path.  This could be useful if the mailbox
      name/path is too long, for instance.  The flag can be used to
      assign or change the label of an existing
      mailbox. <code class="literal">-nolabel</code> is used to remove
      a label from an existing mailbox.
    </p>
    <p>
      In the sidebar, note that
      setting <a href="/doc/manual/#sidebar-sort">$sidebar_sort</a> to
      &quot;alpha&quot; or &quot;name&quot; will sort by the label if
      defined, where as &quot;path&quot; will always sort by the path,
      ignoring labels.
    </p>
    <p>
      <code class="label">-nopoll</code> turns
      off <a href="/doc/manual/#new-mail-polling">new mail polling</a>
      for that mailbox.  This could be helpful if you want to list
      mailboxes in the sidebar for quick access, but don't need new
      mail notifications for them. <code class="literal">-poll</code>
      turns polling back on for an existing mailbox.
    </p>
    <p>
      Both of these flags apply to the mailbox listed after it.  You can
      still list multiple mailboxes, with or without labels/nopoll flags:
    </p>
    <pre>
      mailboxes -label 'Project X' =really/long/mailbox/path \
                =anothermailbox \
                -nopoll -label 'Archive' =really/long/path/to/archive
    </pre>

    <h2>ISO 8601 calendar dates for pattern searching</h2>
    <p>
      Mutt still uses the hyphen for date ranges, but you can now
      specify absolute dates in the form YYYYMMDD for
      <a href="/doc/manual/#date-patterns">date patterns</a>.
    </p>

    <h2>Opportunistic Encryption with Strong Keys</h2>
    <p>
      <a href="/doc/manual/#crypt-opportunistic-encrypt-strong-keys"
      >$crypt_opportunistic_encrypt_strong_keys</a> modifies the
      behavior of $crypt_opportunistic_encrypt to only match against
      &quot;strong keys&quot;, that is, keys with full validity
      according to the web-of-trust algorithm.  Those with large
      keyrings may find this helps make opportunistic encryption more
      useful.
    </p>

    <h2>send2-hook Matching Against Email Contents</h2>
    <p>
      You can now use the <a href="/doc/manual/#patterns">pattern
      modifiers</a> <code class="literal">~b</code>, <code class="literal">~B</code>,
      and <code class="literal">~h</code> with
      <a href="/doc/manual/#send2-hook">send2-hooks</a>.  This can be
      used to make configuration changes based on the message
      contents, such as enabling
      <a href="/doc/manual/#send-multipart-alternative">$send_multipart_alternative</a>
      if the email contains markdown.
    </p>
    <p>
      Note that only the composed message (i.e. the first part) is
      scanned by the pattern modifiers.  Other attachments are not
      scanned.
    </p>

    <hr>

    <h2>Smaller Features/Changes</h2>

    <h3>Compose Menu Movement</h3>
    <p>
      The functions <code class="literal">&lt;move-down&gt;</code> and
      <code class="literal">&lt;move-up&gt;</code> have been added to
      the compose menu.  These allow rearranging the order of the
      various parts.
    </p>

    <h3>Sidebar Navigate to First/Last Entries</h3>
    <p>
      The functions <code class="literal">&lt;sidebar-first&gt;</code>
      and <code class="literal">&lt;sidebar-last&gt;</code> have been
      added to the index and pager menus.
    </p>

    <h3>Attachment Counting of &quot;Root&quot; Parts</h3>
    <p>
      The <a href="/doc/manual/#attachments">attachments</a> command
      previously did not count the first part if it was of inline
      disposition.  This was because those are almost always the
      message contents and so aren't considered interesting to count.
      This also allows specifying inline matching of type text/plain:
    </p>
    <pre>
      attachments   +I text/plain
    </pre>
    <p>
      to match text/plain inline attachments without counting the main
      contents of the email.  (This setting is in fact part of the
      defaults in Mutt's sample muttrc).
    </p>
    <p>
      However, there are some rare instances where you <b>do</b> want
      to count an initial inline part.  For those cases, you can
      specify a disposition of &quot;<code class="literal">root</code>&quot;
      or &quot;<code class="literal">R</code>&quot; to
      the attachments command.
    </p>
    <p>
      Note if you
      have <a href="/doc/manual/#count-alternatives">$count_alternatives</a>
      set: an initial multipart/alternatives' top-level inline parts
      are also counted as type &quot;root&quot;.  Top-level attachment
      parts are still of type &quot;attachment&quot.  Nested parts
      (e.g. inside a multipart/related alternative) are counted
      normally.
    </p>

    <h3>Message/global Attachment Handling</h3>
    <p>
      Mutt now handles viewing &quot;message/global&quot; attachments
      and their parts.  (They are treated the same as
      &quot;message/rfc822&quot;.)
    </p>

    <h3>Copy/Save Tagged Messages Progress Messages</h3>
    <p>
      Copying and saving tagged messages now displays a progress meter
      by the number of tagged messages.  Previously, when copying from
      a local mailbox to IMAP, the progress meter would display for
      each message's upload (byte-count) progress individually, giving
      no indication of how far along the whole process was.
    </p>

    <h3>Decryption errors no long abort displaying the pager</h3>
    <p>
      Decryption issues are now considered "non-fatal", and won't
      abort the message pager.  This may be helpful in the case of a
      nested encrypted message, so you can at least view part of the message.
    </p>
    <p>
      An error message will display when a rendering error occurs:
    </p>
    <p>
      <img src="decrypt-error.png">
    </p>

    <h3>Autoview Reply Formatting</h3>
    <p>
      If your autoview programs output formatting (backspaced
      underline/overstrike, or ansi colors), Mutt will now remove the
      formatting when replying.  This allows you to prettify your
      pager output without adding garbage to the quoted reply text.  I
      believe this change is safe and generally desirable, but if this
      affects you negatively please let me know.
    </p>

    <h3>Cross-Compilation Improvements</h3>
    <p>
      Two build-time tools, which were previously compiled and then
      run as part of the build, have been rewritten in perl:
      hcachever.sh+mutt_md5 → hcachever.pl, and makedoc.c →
      makedoc.pl.
    </p>
    <p>
      This change fixes cross-compilation with the documentation or
      header cache enabled.
    </p>
  </body>
</html>
