<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 2.0 Release Notes</title>
    <meta charset="utf-8"/>
  </head>

  <body>
    <h1>Mutt 2.0 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h2>Backward Incompatible Changes</h2>
    <p>
      This release was bumped to 2.0, not because of the magnitude of
      features (which is actually smaller than past releases), but
      because of a few changes that are backward incompatible.  Please
      read this list carefully to be aware of these changes:
      <ul>
        <li>
          When using <code class="literal">&lt;attach-file&gt;</code> to
          browse and add multiple attachments to an email, you may use
          <code class="literal">&lt;quit&gt;</code> to exit after tagging
          the files.  Previously, &quot;enter&quot; had to be pressed on a
          non-directory file.  This was awkward and non-intuitive.
        </li>
        <li>
          The default value of some configuration variables is
          translatable.  Those are marked with
          type <code class="literal">string (localized)</code> in the
          manual.  Examples
          include <a href="/doc/manual/#attribution">$attribution</a>
          and
          <a href="/doc/manual/#status-format">$status_format</a>.
        </li>
        <li>
          <a href="/doc/manual/#ssl-force-tls">$ssl_force_tls</a>
          defaults set.  If this sounds familiar, that's because I
          tried this before in the 1.13.0 release, but reverted it
          after some breakage.  I'm trying again with a major revision
          number bump.  If you need to connect to a non-encrypted
          site, you will need to turn this setting off yourself.
        </li>
        <li>
          <code class="literal">&lt;decode-copy&gt;</code> and
          <code class="literal">&lt;decode-save&gt;</code> no longer
          perform header weeding by default.  Header weeding can
          be toggled back on by setting
          <a href="/doc/manual/#copy-decode-weed">$copy_decode_weed</a>.
        </li>
        <li>
          <a href="/doc/manual/#hostname">$hostname</a> is set after
          processing the muttrc and <code class="literal">-e</code>
          command-line arguments.  This was done to provide a way to skip
          DNS lookups of the FQDN, which on some systems can result in
          a delay when starting up.
        </li>
        <li>
          <a href="/doc/manual/#reply-to">$reply_to</a> is processed
          before <a href="/doc/manual/#reply-self">$reply_self</a>.
        </li>
        <li>
          Normal configuration variables (as opposed to
          <a href="/doc/manual/#set-myvar">user-defined my variables</a>)
          were previously escaped when used on the right hand side of an
          assignment.  Newline was converted to &quot;\n&quot;, carriage
          return to &quot;\r&quot;, tab to &quot;\t&quot;, backslash and
          double quotes were backslash escaped.  This was a bug, but a
          long-standing one, so I note the change here.
        </li>
      </ul>
    </p>

    <h2>Domain-literal support in email addresses</h2>
    <p>
      This feature isn't commonly needed, but see
      <a href="https://gitlab.com/muttmua/mutt/-/issues/226">ticket 226</a>
      for the reasoning behind the request.  This allows the use of
      literal IP addresses in place of the email address domain.  For example
      <code class="literal">user@[IPv6:fcXX:....]</code>.
    </p>

    <h2>Ability to change directory</h2>
    <p>
      The <code class="literal">cd</code> command allows the ability to change
      the working directory inside Mutt.
    </p>
    <p>
      I believe this wasn't implemented previously because Mutt didn't
      resolve relative paths internally.  So, starting Mutt with
      something like &quot;<code class="literal">mutt -f mymailbox</code>&quot;
      would cause Mutt to open &quot;mymailbox&quot; in the current
      directory and continue to refer to it as &quot;mymailbox&quot;
      internally.  If the directory changed, Mutt would still
      refer (incorrectly) to &quot;mymailbox&quot; in the new directory.
    </p>
    <p>
      Because of this, Mutt now also tries to resolve relative paths.
      This isn't as straightforward as you would think.  So if you
      encounter issues, please let us know!
    </p>

    <h2>Automatic reconnect to IMAP on error</h2>
    <p>
      When an unplanned disconnect occurs, Mutt will try to reconnect
      automatically, preserving unsaved changes in the
      mailbox.  This isn't bulletproof, but will hopefully at least
      reduce lost changes due to a mailbox connection freezing or
      being dropped.
    </p>

    <h2>Protected Headers Improvements</h2>
    <p>
      <a href="/doc/manual/#crypt-protected-headers-subject"
         >$crypt_protected_headers_subject</a> defaults to &quot;...&quot;
      to comply with the recent recommendations.
    </p>
    <p>
      Also, Mutt will store the Date, From, To, Cc, and Reply-To headers
      in protected headers.  Mutt currently doesn't display or make use
      of those headers, but other MUA's may expect them to be there.
    </p>

    <h2>XOAUTH2 Support</h2>
    <p>
      &quot;xoauth2&quot; is supported as a value in
      <a href="/doc/manual/#imap-authenticators">$imap_authenticators</a>,
      <a href="/doc/manual/#smtp-authenticators">$smtp_authenticators</a>, and
      <a href="/doc/manual/#pop-authenticators">$pop_authenticators</a>.
      Additionally there is a
      <a href="https://gitlab.com/muttmua/mutt/tree/master/contrib/mutt_oauth2.py"
         >refresh token script</a> under the contrib directory which
      works with Python 3.  (The script is not officially
      supported by the Mutt team, but has been reported to work.)
    </p>

    <h2>Pattern Completion</h2>
    <p>
      If you sometimes forget a pattern modifier, you can type Tab
      after the ~ to get a list of pattern modifiers.  Hitting enter
      on a selection will add it to the line editor.<br/>
      <img src="pattern-complete.png">
    </p>

    <h2>Decode and Header Weeding</h2>
    <p>
      Three new variables,
      <a href="/doc/manual/#copy-decode-weed">$copy_decode_weed</a>,
      <a href="/doc/manual/#pipe-decode-weed">$pipe_decode_weed</a>, and
      <a href="/doc/manual/#print-decode-weed">$print_decode_weed</a> allow
      you to decouple the operation of "decoding" with the operation of
      header weeding.  Pipe and print operations weed by default, preserving
      previous behavior.  Copy and save operations do not weed by default.
    </p>

    <h2>MuttLisp</h2>
    <p>
      MuttLisp is an experimental feature providing a Lisp-like
      enhancement to the configuration file.  It allows more dynamic
      decisions about commands or command-arguments; however it is not
      a full-blown language, and does not take the place of macros or
      commands.
    </p>
    <p>
      A simple example is the ability to execute commands conditionally:
    </p>
    <pre>
      run (if (equal $USER "kevin8t8") \
            "set arrow_cursor")
    </pre>
    <p>
      or, with <a href="/doc/manual/#muttlisp-inline-eval">$muttlisp_inline_eval</a>
      set, dynamically generate command arguments:
    </p>
    <pre>
      set muttlisp_inline_eval
      set index_format = (if (equal $sidebar_visible "yes") \
        "short index format" \
        "long index format")
    </pre>
    <p>
      For a more detailed introduction and examples, please see the
      <a href="/doc/manual/#muttlisp">MuttLisp manual section</a>.
    </p>

    <h2>Cursor Overlay</h2>
    <p>
      <a href="/doc/manual/#cursor-overlay">$cursor_overlay</a> can be
      used to have an &quot;underline&quot; indicator, for instance,
      that shows the colors of the selected line underneath.
      <code class="literal">default</code> indicator
      foreground/background colors will be set by the color of the
      line instead.  Attributes (such as bold, underline, reverse)
      will be merged between the two.
    </p>
    <p>
      For example:
    </p>
    <pre>
      color indicator underline default default
      color index red default ~N
      set cursor_overlay
    </pre>
    <p>
      <img src="cursor-overlay.png"></br>
      With $cursor_overlay set, the &quot;underline&quot; cursor will
      show the red foreground of a new message.
    </p>

    <h2>Default Attachment Save Directory</h2>
    <p>
      <a href="/doc/manual/#attach-save-dir">$attach_save_dir</a>
      specifies a directory to use when saving attachments.  Note: the
      prompt will be the same, but relative path files will be saved
      relative to that directory.
    </p>

  </body>
</html>
